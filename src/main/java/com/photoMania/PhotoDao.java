package com.photoMania;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
@Repository
public class PhotoDao {
	@Autowired
	private PhotoRepository photoRepository;
	
	public  PhotoModels savePhotos(PhotoModels photoModels) {
		return photoRepository.save(photoModels);
	}
	
	public List<String>  getPhotos(int phototId) {
	  return photoRepository.findNameById(phototId);
		
	}
}
