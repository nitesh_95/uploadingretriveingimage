package com.photoMania;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
@ComponentScan(basePackages = {"com.photoMania"})
public class PhotoMania1Application {

	public static void main(String[] args) {
		SpringApplication.run(PhotoMania1Application.class, args);
	}

}
