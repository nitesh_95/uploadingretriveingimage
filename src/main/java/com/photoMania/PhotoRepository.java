package com.photoMania;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
@Repository
public interface PhotoRepository extends JpaRepository<PhotoModels, Integer> {

/*	@Query("from PhotoModels where photos_id=?")
	PhotoModels getPhotos(int phototId);
*/	
	 public List<PhotoModels> findAllById(int phototId);
	   @Query("SELECT pm.filename FROM PhotoModels pm where pm.photoId = :photos_id") 
	    List<String> findNameById(@Param("photos_id") int phototId);
	   
	  

}
