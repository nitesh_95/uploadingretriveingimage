package com.photoMania;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
@RestController
@CrossOrigin
public class PhotosUploadController {

	@Autowired
	private Environment environment;

	@Autowired
	private PhotoDao photoDao;


	@RequestMapping(value = "/uploadPhoto",method = RequestMethod.POST,consumes = { "multipart/form-data" })
	public LoginReponse savePreweddingPhoto(@RequestParam("file") MultipartFile[] file,@RequestParam Integer photoId) throws IOException {
		LoginReponse res=new LoginReponse(); 
		System.out.println(file);
		for (MultipartFile photo : file) {
			String filename = photo.getOriginalFilename();
			System.out.println("filename--->"+filename);
			String photoPath = "";
			if(photoId == Integer.parseInt(environment.getProperty("birthday.photo"))){
				photoPath =  environment.getProperty("birthday.photo.path")+"\\"+filename ;	
			}else if(photoId == Integer.parseInt(environment.getProperty("prewedding.photo"))) {
				photoPath =  environment.getProperty("prewedding.photo.path")+"\\"+filename ;	
			}else if(photoId == Integer.parseInt(environment.getProperty("wedding_photography.photo"))){
				photoPath =  environment.getProperty("wedding_photography.photo.path")+"\\"+filename ;	
			}else if(photoId == Integer.parseInt(environment.getProperty("fashion_male.photo"))) {
				photoPath =  environment.getProperty("fashion_male.photo.path")+"\\"+filename ;	
			}else if(photoId == Integer.parseInt(environment.getProperty("fashion_female.photo"))){
				photoPath =  environment.getProperty("fashion_female.photo.path")+"\\"+filename ;	
			}else if(photoId == Integer.parseInt(environment.getProperty("celebrity.photo"))) {
				photoPath =  environment.getProperty("celebrity.photo.path")+"\\"+filename ;	
			}else if(photoId == Integer.parseInt(environment.getProperty("commercial.photo"))) {
				photoPath =  environment.getProperty("commercial.photo.path")+"\\"+filename ;	
			}
			try {
				byte barr[] = photo.getBytes();
				BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream(photoPath));
				bout.write(barr);
				bout.flush();
				bout.close();
				// save image path
				PhotoModels pm = null;
				if (photoPath != null && photoPath.trim().length() > 0) {
					pm = new PhotoModels();
					pm.setImagePath(photoPath);
					pm.setPhotoId(photoId);
					pm.setCreatedDate(new Date());
					pm.setFilename(filename);
					PhotoModels photoModels = photoDao.savePhotos(pm);
				}

			} catch (FileNotFoundException e) {
				res.setReason("data not saved");
				res.setStatus("s01");
				e.printStackTrace();
			}
		}
		res.setReason("sucess");
		res.setStatus("00");
		return res;

	}

	@RequestMapping(path = { "/getPhoto" },produces = MediaType.IMAGE_JPEG_VALUE)
	public  ResponseEntity<JSONArray> getImage(@RequestBody Object request) throws IOException {
		System.out.println("inside method");
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonInString = objectMapper.writeValueAsString(request);
		JSONObject obj=new JSONObject(jsonInString);
		List<String> photos = photoDao.getPhotos(obj.getInt("photoId"));
		BufferedImage bImage = null;
		ByteArrayOutputStream bos = null;
		ResponseEntity<byte[]> body = null;
		byte[] data  = null;
		ResponseEntity<JSONObject> response=null;
		JSONArray imagesBytes = new JSONArray();
		
		for (String photo : photos) {
			System.out.println(photo);
			File img = new File(photo);
			obj=new JSONObject();
			bImage = ImageIO.read(new File(photo));
			bos = new ByteArrayOutputStream();
			ImageIO.write(bImage, "jpg", bos );
			data = bos.toByteArray();
			imagesBytes.put(data);
			obj.put("imagesPath", imagesBytes) ;

		} 
		return	new ResponseEntity(obj.toString(),HttpStatus.OK);
	}

		@RequestMapping(path = {"/getPhotoPath"})
		public  ResponseEntity<JSONArray> getPhotoPath(@RequestBody Object request) throws IOException {
			System.out.println("inside method");
			ObjectMapper objectMapper = new ObjectMapper();
			String jsonInString = objectMapper.writeValueAsString(request);
			JSONObject obj=new JSONObject(jsonInString);
			List<String> photos = photoDao.getPhotos(obj.getInt("photoId"));
			BufferedImage bImage = null;
			ByteArrayOutputStream bos = null;
			ResponseEntity<byte[]> body = null;
			byte[] data  = null;
			ResponseEntity<JSONObject> response=null;
			JSONArray imagesPath = new JSONArray();
			
			for (String photo : photos) {
				System.out.println(photo);

				imagesPath.put(photo);
				obj.put("imagesPath", imagesPath) ;

			} 
			return	new ResponseEntity(obj.toString(),HttpStatus.OK);



	}
}





